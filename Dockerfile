FROM debian:9 as stage

RUN apt update && apt install -y wget gcc make git
RUN wget http://nginx.org/download/nginx-1.0.5.tar.gz && tar xvfz nginx-1.0.5.tar.gz && cd nginx-1.0.5 && ./configure --without-http_rewrite_module --without-http_gzip_module  && make && make install 
FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=stage /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
COPY nginx.conf /usr/local/nginx/conf/
CMD ["./nginx"]
